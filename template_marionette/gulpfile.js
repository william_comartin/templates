'use strict';

var browserify = require('browserify'),
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    source = require('vinyl-source-stream'),
    pkg = require('./package.json'),
    connect = require('gulp-connect'),
    dirSync = require('gulp-dir-sync'),
    sftp = require('gulp-sftp');



gulp.task('js', function(){
  browserify({
      entries: ['./src/javascript/application.js'],
      debug: true
    })
    .bundle()
    .pipe(source('app.js'))
    .pipe(gulp.dest('./dist/js'));
});



gulp.task('css', function () {

  // Application CSS Files
  gulp.src('./src/stylesheets/*.scss')
    .pipe(sourcemaps.init())
      .pipe(sass({ style: 'compressed' }))
    .pipe(concat('app.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/css/'));

  // Vendor CSS Files
  gulp.src(pkg.css_imports)
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest('./dist/css/'));

});


gulp.task('sync', function () {

  gulp.src('./src/html/*.*')
    .pipe(gulp.dest('./dist'));

  gulp.src('./src/images/**/*.*')
    .pipe(gulp.dest('./dist/images'));

  gulp.src('./src/templates/*.*')
    .pipe(gulp.dest('./dist/templates'));

})


gulp.task('connect', function() {
  connect.server({
    root: './dist',
    livereload: true
  });
});



gulp.task('reload', function () {
  connect.reload();
})


gulp.task('sftp_staging', function () {
  gulp.src('./dist/*')
    .pipe(sftp({
      host: pkg.staging.host,
      remotePath: pkg.staging.remote_path
    }))
})
gulp.task('sftp_production', function () {
  gulp.src('./dist/*')
    .pipe(sftp({
      host: pkg.production.host,
      remotePath: pkg.production.remote_path
    }))
})



gulp.task('watch', function () {
  gulp.watch(['./src/javascript/**/*.js'], ['js']);
  gulp.watch(['./src/stylesheets/*.scss'], ['css']);
  gulp.watch(['./src/html/*.*',
              './src/images/**/*.*',
              './src/templates/*.*'], ['sync']);
  gulp.watch(['./dist/**/*.*'], ['reload'])
});



gulp.task('default', ['js', 'css', 'sync', 'connect', 'watch']);
gulp.task('stage', ['js', 'css', 'sync', 'sftp_staging']);
gulp.task('deploy', ['js', 'css', 'sync', 'sftp_production']);
