angular.module('app', ['ui.bootstrap', 'dialogs.main'])
  .config([function(){

  }])
  .run(['$rootScope', function($rootScope){

    $.ajaxSetup({
      beforeSend: function(xhr){
        xhr.setRequestHeader("Authorization", "Basic " + $rootScope.token)
      }
    })

  }])
  .directive('buttons', [function(){
    return {
      restrict: 'E',
      scope: '=',
      link: function(scope, element, attrs){

        console.log('here')
        scope.message = "Message Here"

        scope.login = function(){

          scope.token = btoa(scope.username + ":" + scope.password);

          $.ajax({
            type: 'POST',
            url: 'http://localhost:4567/login',
            dataType: 'json'
          })
          .always(function(data){
            scope.message = data.message
            scope.$apply();
            loggedin = true;
          })
        }

        scope.logout = function(){
          $.ajax({
            type: 'POST',
            url: 'http://localhost:4567/logout',
            dataType: 'json'
          })
          .always(function(data){
            scope.message = data.message
            delete scope.token
            scope.$apply();
          })
        }

        scope.protected = function(){
          $.ajax({
            type: 'GET',
            url: 'http://localhost:4567/protected',
            dataType: 'json'
          })
          .always(function(data){
            scope.message = data.message
            scope.$apply();
          })
        }

        scope.unprotected = function(){
          $.ajax({
            type: 'GET',
            url: 'http://localhost:4567/',
            dataType: 'json'
          })
          .always(function(data){
            scope.message = data.message
            scope.$apply();
          })
        }

        scope.register = function(){
          $.ajax({
            type: 'POST',
            url: 'http://localhost:4567/register',
            dataType: 'json',
            data: {
              username: scope.username,
              password: scope.password
            }
          })
          .always(function(data){
            scope.token = btoa(scope.username + ":" + scope.password);
            scope.message = data.message
            scope.$apply();
          })
        }


      }
    }
  }])
