#encoding: UTF-8

get '/api/users' do
    format_response(User.all, request.accept)
end

get '/api/users/:id' do
    user ||= User.get(params[:id]) || halt(404)
    format_response(user, request.accept)
end