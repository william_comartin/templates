helpers do
  def protected!
    return if authorized?
    headers['WWW-Authenticate'] = 'Basic realm="Restricted Area"'
    halt 401, format_response({"message": "Not Authorized"}, request.accept)
  end

  def authorized?
    @auth ||=  Rack::Auth::Basic::Request.new(request.env)
    @auth.provided? and @auth.basic? and @auth.credentials and checkcreds(@auth.credentials)
  end

  def checkcreds(creds)
    creds[0] == "admin" and creds[1] == "password"
  end
end

post '/logout' do
  format_response({"message": "Logged Out"}, request.accept)
end

post '/login' do
  protected!
  format_response({"message": "Logged In"}, request.accept)
end

post '/register' do
  format_response({
    "message": "Registered #{params[:username]}"
    }, request.accept)
end