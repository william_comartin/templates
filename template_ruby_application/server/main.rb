# encoding: UTF-8
require 'json'
require 'sinatra'
require 'data_mapper'
require 'dm-migrations'
require 'sinatra/cross_origin'

configure :development do
  print "Development\n"
  enable :cross_origin
  DataMapper::Logger.new($stdout, :debug)

  DataMapper::setup(:default, "sqlite3://#{Dir.pwd}/database.db")

end

configure :production do
  print "Production\n"
  enable :cross_origin

  DataMapper.setup(:default, 'postgres://postgres:user@host/db_name')

end

get '/' do
  format_response({"message": "Hello World"}, request.accept)
end

get '/protected' do
  protected!
  format_response({"message": "You're In!"}, request.accept)
end

require './models/init'
require './helpers/init'
require './resources/init'

DataMapper.finalize
