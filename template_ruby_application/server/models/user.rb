# encoding: UTF-8
class User
  include DataMapper::Resource

  property :id,         Serial
  property :email,      String
  property :password,   String
  property :role,       String
end