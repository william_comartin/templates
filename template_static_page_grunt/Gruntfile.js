module.exports = function(grunt) {

  var pkgJson = require('./package.json');

  grunt.initConfig({

    pkg: pkgJson,


    'concat': {
      stylesheets: {
        src: [
          '.temp/application.css'
        ],
        dest: '.temp/application.concat.css',
      },
      vendor_css: {
        src: pkgJson.stylesheetDependancies,
        dest: '.temp/vendor.concat.css'
      }
    },

    browserify: {
      development: {
        files: {
          'dist/js/app.min.js': ['src/javascript/application.js'],
        },
        options: {
          browserifyOptions: {
             debug: true
          }
        }
      },
      production: {
        files: {
          '.temp/application.concat.js': ['src/javascript/application.js'],
        }
      }
    },

    'sync': {
      main: {
        files: [
          // includes files within path and its sub-directories
          {expand: true, cwd: 'src/images', src: ['**', '!**.pxm'], dest: 'dist/images'},
          {expand: true, cwd: 'src', src: ['**.*'], dest: 'dist'},
        ],
        updateOnly: true
      }
    },

    'cssmin': {
      application: {
        options: {
          banner: '/* Minified CSS - DO NOT MODIFY THIS FILE DIRECTLY */'
        },
        files: {
          '<%= pkg.development.local_path %>/css/app.min.css': ['.temp/application.concat.css'],
        }
      },
      vendor: {
        options: {
          banner: '/* Minified CSS - DO NOT MODIFY THIS FILE DIRECTLY */'
        },
        files: {
          '<%= pkg.development.local_path %>/css/vendor.min.css': ['.temp/vendor.concat.css']
        }
      }
    },

    'sass': {
      development: {
        options: {
          style: 'expanded'
        },
        files: {
          '.temp/application.css': 'src/stylesheets/application.scss'
        }
      },
      production: {
        options: {
          style: 'compressed'
        },
        files: {
          '.temp/application.css': 'src/stylesheets/application.scss'
        }
      }
    },

    'sftp-deploy': {
      development: {
        auth: {
          host: '<%= pkg.development.host %>',
          port: 22,
          authKey: 'development'
        },
        cache: '.temp/sftpCache-development.json',
        src: '<%= pkg.development.local_path %>',
        dest: '<%= pkg.development.remote_path %>',
        exclusions: ['*.pxm', '*.DS_Store', 'Thumbs.db'],
        concurrency: 4,
        progress: false
      },
      staging: {
        auth: {
          host: '<%= pkg.staging.host %>',
          port: 22,
          authKey: 'staging'
        },
        cache: '.temp/sftpCache-staging.json',
        src: '<%= pkg.staging.local_path %>',
        dest: '<%= pkg.staging.remote_path %>',
        exclusions: ['*.pxm', '*.DS_Store', 'Thumbs.db'],
        concurrency: 4,
        progress: false
      },
      production: {
        auth: {
          host: '<%= pkg.production.host %>',
          port: 22,
          authKey: 'production'
        },
        cache: '.temp/sftpCache-production.json',
        src: '<%= pkg.production.local_path %>',
        dest: '<%= pkg.production.remote_path %>',
        exclusions: ['*.pxm', '*.DS_Store', 'Thumbs.db'],
        concurrency: 4,
        progress: false
      }
    },

    'uglify': {
      production: {
        options: {
          beautify: false
        },
        files: {
          'dist/app.min.js': ['.temp/application.concat.js']
        }
      }
    },

    'watch': {
      scripts: {
        files: ['src/javascript/**'],
        tasks: ['browserify:development', 'watch'],
        options: {
          spawn: false,
        }
      },
      stylesheets: {
        files: ['src/stylesheets/*.scss'],
        tasks: ['sass:development','concat_css','watch'],
        options: {
          spawn: false,
        }
      },
      html: {
        files: ['src/**.*', 'src/images/**.*'],
        tasks: ['sync','watch'],
        options: {
          spawn: false,
        }
      }
      // templates: {
      //   files: ['src/templates/**/*.hbs'],
      //   tasks: ['handlebars', 'concat:javascript', 'uglify:development', 'watch'],
      //   options: {
      //     spawn: false,
      //   }
      // }
    },

    'connect': {
      server: {
        options: {
          port: 8000,
          base: 'dist'
        }
      }
    }

  });


  // Load Plugins
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sftp-deploy');
  grunt.loadNpmTasks('grunt-sync');


  // Register Custom Tasks
  grunt.registerTask('default',    ['build',
                                    'connect:server',
                                    'watch'
                                   ]);

  grunt.registerTask('build',      ['browserify:development',
                                    'sass:development',
                                    'concat_css',
                                    'sync'
                                   ]);

  grunt.registerTask('concat_css', ['concat:stylesheets',
                                    'concat:vendor_css',
                                    'cssmin'
                                   ]);

  grunt.registerTask('stage',      ['build',
                                    'sftp-deploy:staging'
                                   ]);

  grunt.registerTask('deploy',     ['browserify:production',
                                    'uglify:production',
                                    'sass:production',
                                    'concat_css',
                                    'sync',
                                    'sftp-deploy:production'
                                   ]);

};
