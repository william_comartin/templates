var pkg = require('./package.json'),
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    source = require('vinyl-source-stream'),
    connect = require('gulp-connect'),
    sftp = require('gulp-sftp'),
    templateCache = require('gulp-angular-templatecache'),
    livereload = require('gulp-livereload'),
    gettext = require('gulp-angular-gettext'),
    rsync = require('gulp-rsync');

gulp.task('js', function(){

  gulp.src(['./src/javascript/application.js'])
    .pipe(sourcemaps.init())
    .pipe(concat('app.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./www/js'))
    .pipe(livereload());

  gulp.src(pkg.bowerFiles.javascript)
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('./www/js'))
    .pipe(livereload());

});

gulp.task('templates', function(){

  gulp.src('./src/templates/**/*.html')
    .pipe(templateCache('templates.js', {
      standalone: true
    }))
    .pipe(gulp.dest('./www/js'))
    .pipe(livereload());

});

gulp.task('css', function(){

  // Application CSS Files
  gulp.src('./src/stylesheets/*.scss')
    .pipe(sourcemaps.init())
      .pipe(sass({ style: 'compressed' }))
    .pipe(concat('app.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./www/css/'))
    .pipe(livereload());

  // Vendor CSS Files
  gulp.src(pkg.bowerFiles.stylesheets)
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest('./www/css/'))
    .pipe(livereload());

});

gulp.task('sync', function () {

  gulp.src('./src/*.html')
    .pipe(gulp.dest('./www'))
    .pipe(livereload());

  gulp.src('./src/images/**/*.*')
    .pipe(gulp.dest('./www/images'))
    .pipe(livereload());

  gulp.src('./bower_components/font-awesome/fonts/**/*.*')
    .pipe(gulp.dest('./www/fonts'))
    .pipe(livereload());

  gulp.src('./res/**/*.*')
    .pipe(gulp.dest('./www/res'))
    .pipe(livereload());

});

gulp.task('connect', function() {

  connect.server({
    root: './www',
    // livereload: true,
    port: 8000
  });
});

gulp.task('reload', function () {
  connect.reload();
});


gulp.task('watch', function () {
  livereload.listen();
  gulp.watch(['./src/javascript/**/*.js'], ['js']);
  gulp.watch(['./src/javascript/**/*.html'], ['templates']);
  gulp.watch(['./src/stylesheets/**/*.scss'], ['css']);
  gulp.watch(['./src/*.html',
              './src/images/**/*.*',
              './src/config/**/*.*'], ['sync']);
});

gulp.task('build', ['js', 'templates', 'css', 'sync']);

gulp.task('default', ['build', 'connect', 'watch']);
