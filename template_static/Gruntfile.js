module.exports = function(grunt) {

  var pkgJson = require('./package.json');

  grunt.initConfig({

    pkg: pkgJson,


    'concat': {
      development: {
        files: {
          'dist/js/app.min.js': ['src/javascript/application.js'],
          'dist/js/vendor.min.js': pkgJson.bowerFiles.javascript
        }
      },
      production: {
        files: {
          '.temp/application.concat.js': ['src/javascript/application.js'],
          '.temp/vendor.concat.js': pkgJson.bowerFiles.javascript
        }
      },
      css: {
        files: {
          '.temp/application.concat.css': ['.temp/application.css'],
          '.temp/vendor.concat.css': pkgJson.bowerFiles.stylesheets
        }
      }

    },

    'sync': {
      main: {
        files: [
          // includes files within path and its sub-directories
          {expand: true, cwd: 'src/images', src: ['**', '!**.pxm'], dest: 'dist/images'},
          {expand: true, cwd: 'src/fonts', src: ['**'], dest: 'dist/fonts'},
          {expand: true, cwd: 'src/templates', src: ['**.html', '**.mst'], dest: 'dist/templates'},
          {expand: true, cwd: 'src', src: ['**.*'], dest: 'dist'},

        ].concat(pkgJson.bowerFiles.others),
        updateOnly: true
      }
    },

    'cssmin': {
      application: {
        options: {
          banner: '/* Minified CSS - DO NOT MODIFY THIS FILE DIRECTLY */'
        },
        files: {
          '<%= pkg.development.local_path %>/css/app.min.css': ['.temp/application.concat.css']
        }
      },
      vendor: {
        options: {
          banner: '/* Minified CSS - DO NOT MODIFY THIS FILE DIRECTLY */'
        },
        files: {
          '<%= pkg.development.local_path %>/css/vendor.min.css': ['.temp/vendor.concat.css']
        }
      }
    },

    'sass': {
      development: {
        options: {
          style: 'expanded'
        },
        files: {
          '.temp/application.css': 'src/stylesheets/application.scss'
        }
      },
      staging: {
        options: {
          style: 'compressed'
        },
        files: {
          '.temp/application.css': 'src/stylesheets/application.scss'
        }
      },
      production: {
        options: {
          style: 'compressed'
        },
        files: {
          '.temp/application.css': 'src/stylesheets/application.scss'
        }
      }
    },

    'sftp-deploy': {
      development: {
        auth: {
          host: '<%= pkg.development.host %>',
          port: 22,
          authKey: 'development'
        },
        cache: '.temp/sftpCache-development.json',
        src: '<%= pkg.development.local_path %>',
        dest: '<%= pkg.development.remote_path %>',
        exclusions: ['*.pxm', '*.DS_Store', 'Thumbs.db'],
        concurrency: 4,
        progress: false
      },
      staging: {
        auth: {
          host: '<%= pkg.staging.host %>',
          port: 22,
          authKey: 'staging'
        },
        cache: '.temp/sftpCache-staging.json',
        src: '<%= pkg.staging.local_path %>',
        dest: '<%= pkg.staging.remote_path %>',
        exclusions: ['*.pxm', '*.DS_Store', 'Thumbs.db'],
        concurrency: 4,
        progress: false
      },
      production: {
        auth: {
          host: '<%= pkg.production.host %>',
          port: 22,
          authKey: 'production'
        },
        cache: '.temp/sftpCache-production.json',
        src: '<%= pkg.production.local_path %>',
        dest: '<%= pkg.production.remote_path %>',
        exclusions: ['*.pxm', '*.DS_Store', 'Thumbs.db'],
        concurrency: 4,
        progress: false
      }
    },

    'uglify': {
      options: {
        beautify: false
      },
      files: {
        'dist/js/app.min.js': ['.temp/application.concat.js'],
        'dist/js/vendor.min.js': ['.temp/vendor.concat.js']
      }
    },

    'watch': {
      scripts: {
        files: ['src/javascript/**'],
        tasks: ['concat:development', 'watch'],
        options: {
          spawn: false,
        }
      },
      stylesheets: {
        files: ['src/stylesheets/*.scss'],
        tasks: ['sass:development','concat:css','cssmin','watch'],
        options: {
          spawn: false,
        }
      },
      php: {
        files: ['src/**.*', 'src/images/**.*', 'src/templates/**.*'],
        tasks: ['sync','watch'],
        options: {
          spawn: false,
        }
      }
      // md: {
      //   files: ['src/docs/*.md'],
      //   tasks: ['md'],
      //   options: {
      //     spawn: false
      //   }
      // }
    },

    'connect': {
      server: {
        options: {
          livereload: false,
          port: 8000,
          base: 'dist'
        }
      }
    },

    markdown: {
      all: {
        files: [
          {
            expand: true,
            cwd: 'src/docs',
            src: '*.md',
            dest: '.temp/docs/',
            ext: '.html'
          }
        ],
        options: {
          template: 'mdTemplate.jst',
          postCompile: function(src, context){
            src = src.replace("<table>", "<table class='table'>");
            return src;
          }
        }
      }
    },

    prettify: {
      all: {
        expand: true,
        cwd: '.temp/docs/',
        ext: '.html',
        src: ['*.html'],
        dest: 'dist/docs/'
      }
    }

  });

  // Load Plugins
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sftp-deploy');
  grunt.loadNpmTasks('grunt-sync');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-markdown');
  grunt.loadNpmTasks('grunt-prettify');

  // Default task creates a fresh build
  grunt.registerTask('default', ['build', 'connect:server', 'watch']);

  grunt.registerTask('md', ['markdown', 'prettify']);

  grunt.registerTask('build', ['concat:development',
                               'sass:development',
                               'concat:css',
                               'cssmin',
                               'sync']);

  grunt.registerTask('develop', ['build', 'sftp-deploy:development']);

  grunt.registerTask('stage', ['concat:development',
                               'sass:staging',
                               'concat:css',
                               'cssmin',
                               'sync',
                               'sftp-deploy:staging']);
  grunt.registerTask('deploy', ['concat:production',
                                'uglify',
                                'sass:production',
                                'concat:css',
                                'cssmin',
                                'sync',
                                'sftp-deploy:production']);

};
