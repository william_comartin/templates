var templates = {};


var renderTemplate = function(template, data) {
    if(!(template in templates)){

        templates[template] = $.ajax({
            type: "GET",
            url: "templates/" + template,
            async: false
        }).responseText;

        Mustache.parse(templates[template]);

    }
    return Mustache.render(templates[template], data);
}

var setupPopup = function(feature, popupTemplate, className){

    return renderTemplate(popupTemplate, {
        feature:feature,
        className:className
    });
}

var mapLoaded = function(){

    //Layers Added Here


};


/*  */
$(document).ready(function() {

    window.map = new Xenmap({
        element: "map",
        baseUrl: "http://ec2-54-204-120-130.compute-1.amazonaws.com/v2/WE-Roads.json",
        bound:false,
        zoom: 6
    });
    map.init(mapLoaded);

});
